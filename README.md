# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* 簡易執行

```bash
cd ./item-counter
mvn clean install -Dmaven.test.skip=true
docker-compose up -d
```



### 目錄結構

```bash
.
├── counter-front          # 前端程式
└── item-counter           # 後端程式
    ├── counter-api        # api目錄
    ├── counter-batch      # 排程目錄
    ├── counter-common     # 共用lib
    ├── counter-database   # db相關的entity
    ├── counter-kafka      # kafka stream等等的執行
    ├── eureka             # spring cloud提供註冊的server
    ├── gateway            # 於eureka註冊後可在gateway看到api
    └── sql                # 環境建立時，需要執行的DDL與DML

```



### Who do I talk to? ###

* lucas.wang@evolutivelabs.com
* https://sites.google.com/evolutivelabs.com/hrsop?pli=1&authuser=0