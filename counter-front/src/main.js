import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import alert from './plugins/alert'
import axios from './plugins/axios'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import bus from './plugins/bus'

library.add(fas, far)

const app = createApp(App)
app.component('font-awesome-icon', FontAwesomeIcon)
.use(axios)
.use(router)
.use(alert)
.use(bus)
.mount('#app')
