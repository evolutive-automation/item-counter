import swal from 'sweetalert2'

const _swal = function(title, msg) {
       showAlert(null, title, msg)
}

_swal.error = function(title, msg, destory) {
    showAlert('error', title, msg, destory)
}

_swal.success = function(title, msg) {
    showAlert('success', title, msg)
}
_swal.warning = function(title, msg) {
    showAlert('warning', title, msg)
}
_swal.info = function(title, msg) {
    showAlert('info', title, msg)
}
_swal.question = function(title, msg) {
    showAlert('quention', title, msg)
}

const showAlert = function(icon, title, msg, destory) {
    swal.fire({
        icon,
        title,
        text: msg,
        didDestroy: destory ? destory() : null
    })
}

export const alert = _swal
export default {
    install: function(Vue) {
        Object.defineProperties(Vue.config.globalProperties, {
            $alert: {
                get() {
                    return _swal
                }
            }
        })
    }
}