"use strict";

import axios from "axios";
import router from "../router"
import { alert } from "./alert"

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

let config = {
  // baseURL: process.env.baseURL || process.env.apiUrl || ""
  baseURL: 'http://localhost:8082/counter-batch'
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
};

const _axios = axios.create(config);

_axios.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    config.headers.authorization = sessionStorage.getItem('jwt_token')
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
_axios.interceptors.response.use(
  function(response) {
    // Do something with response data
    return response;
  },
  function(error) {
    if (error && error.response && error.response.status) {
      if (error.response.status == 403 || error.response.status == 401) {
        sessionStorage.removeItem('jwt_token')
        // console.log(alert)
        alert.error(error.response.status, error.response.data, () => {router.push('/login')})
      }
    }
    // Do something with response error
    return Promise.reject(error);
  }
);

export default {
  install: function(Vue) {
    Object.defineProperties(Vue.config.globalProperties, {
      $axios: {
        get() {
          return _axios;
        }
      }
    })
  }
}

