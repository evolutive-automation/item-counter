import mitt from "mitt"

const emitter = mitt()

export default {
    install: function(Vue) {
        console.log("axsaas")
        Object.defineProperties(Vue.config.globalProperties, {
            $bus: {
                get() {
                    return emitter
                }
            }
        })
    }
}